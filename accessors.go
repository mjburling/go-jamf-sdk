package jamf

/* Category Accessors */

// GetID returns the id of the given Script
func (c *Category) GetID() int {
	if c == nil || c.ID == nil {
		return 0
	}
	return *c.ID
}

// GetPriority returns the priority of the given script
func (c *Category) GetPriority() string {
	if c == nil || c.Priority == nil {
		return ""
	}
	return *c.Priority
}

// GetName returns the name for the given script
func (c *Category) GetName() string {
	if c == nil || c.Name == nil {
		return ""
	}
	return *c.Name
}

/* Script Accessors */

// GetID returns the id of the given Script
func (s *Script) GetID() int {
	if s == nil || s.ID == nil {
		return 0
	}
	return *s.ID
}

// GetName returns the name for the given script
func (s *Script) GetName() string {
	if s == nil || s.Name == nil {
		return ""
	}
	return *s.Name
}

// GetCategory returns category for the given script
func (s *Script) GetCategory() string {
	if s == nil || s.Category == nil {
		return ""
	}
	return *s.Category
}

// GetFilename returns the filename for the given script
func (s *Script) GetFilename() string {
	if s == nil || s.Filename == nil {
		return *s.Name
	}
	return *s.Filename
}

// GetInfo returns the info for the given script
func (s *Script) GetInfo() string {
	if s == nil || s.Info == nil {
		return ""
	}
	return *s.Info
}

// GetNotes returns the notes for a given script
func (s *Script) GetNotes() string {
	if s == nil || s.Notes == nil {
		return ""
	}
	return *s.Notes
}

// GetPriority returns the priority of the given script
func (s *Script) GetPriority() string {
	if s == nil || s.Priority == nil {
		return ""
	}
	return *s.Priority
}

// GetOSRequirements returns the os_requirements for the given script
func (s *Script) GetOSRequirements() string {
	if s == nil || s.OSRequirements == nil {
		return ""
	}
	return *s.OSRequirements
}

// GetScriptContents returns the script_contents of a given script
func (s *Script) GetScriptContents() string {
	if s == nil || s.ScriptContents == nil {
		return ""
	}
	return *s.ScriptContents
}

// GetScriptContentsEncoded returns the script_contents of a given script
func (s *Script) GetScriptContentsEncoded() string {
	if s == nil || s.ScriptContentsEncoded == nil {
		return ""
	}
	return *s.ScriptContentsEncoded
}
