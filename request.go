package jamf

import (
	"bytes"
	"encoding/xml"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/http/httputil"
	"net/url"
	"strings"
)

// uriForAPI returns a full URI for a given api endpoint
func (client *Client) uriForAPI(api string) (string, error) {
	var err error
	// If api is a URI such as /accounts or /scripts, add credentials and return a properly formatted URL
	if !(strings.HasPrefix(api, "https://") || strings.HasPrefix(api, "http://")) {
		apiBase, err := url.Parse(client.GetBaseURL() + "/JSSResource" + api)
		if err != nil {
			return "", err
		}
		return apiBase.String(), nil
	}
	// if api is a generic URL we simply return it
	apiBase, err := url.Parse(api)
	if err != nil {
		return "", err
	}
	return apiBase.String(), nil
}

// redactError removes sensitive values from error strings
// TODO verify that this is necessary fro the Jamf API
func (client *Client) redactError(err error) error {
	if err == nil {
		return nil
	}
	errString := err.Error()

	if len(client.username) > 0 {
		errString = strings.Replace(errString, client.username, "redacted", -1)
	}
	if len(client.password) > 0 {
		errString = strings.Replace(errString, client.password, "redacted", -1)
	}

	// Return original error if no replacements were made to keep the original,
	// probably more useful error type information.
	if errString == err.Error() {
		return err
	}
	return fmt.Errorf("%s", errString)
}

// doXMLRequest wraps dispatches to doXMLRequestUnredacted and redactError as necessary
func (client *Client) doXMLRequest(method, api string,
	reqbody, out interface{}) error {
	if err := client.doXMLRequestUnredacted(method, api, reqbody, out); err != nil {
		return client.redactError(err)
	}
	return nil
}

// doXMLRequestUnredacted is the simplest type of request: a method on a URI that returns
// some XML result which we unmarshal into the passed interface.
func (client *Client) doXMLRequestUnredacted(method, api string,
	reqbody, out interface{}) error {
	req, err := client.createRequest(method, api, reqbody)
	if err != nil {
		return err
	}

	req.Header.Set("Accept", "application/xml")

	// TODO implement a client.Debug or similar for requestDump
	requestDump, err := httputil.DumpRequest(req, true)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(string(requestDump))

	// Perform the request and retry it if it's not a POST or PUT request
	var resp *http.Response
	resp, err = client.HTTPClient.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode < 200 || resp.StatusCode > 299 {
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return err
		}
		return fmt.Errorf("API error %s: %s", resp.Status, body)
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	// TODO implement a client.Debug or similar for bodyString
	bodyString := string(body)
	fmt.Println(bodyString)

	// If we got no body, by default let's just make an empty JSON dict. This
	// saves us some work in other parts of the code.
	if len(body) == 0 {
		body = []byte{'{', '}'}
	}

	return xml.Unmarshal(body, &out)
}

// createRequest returns httpNewRequest to facilitate CRUD actions in Jamf
func (client *Client) createRequest(method, api string, reqbody interface{}) (*http.Request, error) {
	// Handle the body if they gave us one.
	var bodyReader io.Reader
	if method != "GET" && reqbody != nil {
		bxml, err := xml.Marshal(reqbody)
		if err != nil {
			return nil, err
		}
		bodyReader = bytes.NewReader(bxml)
	}

	apiURLStr, err := client.uriForAPI(api)
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest(method, apiURLStr, bodyReader)
	if err != nil {
		return nil, err
	}

	req.SetBasicAuth(client.username, client.password)

	return req, nil
}
