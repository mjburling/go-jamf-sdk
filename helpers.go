package jamf

// Int returns the point to newly allocated int
func Int(i int) *int { return &i }

// String returns the a pointer to newly allocated string
func String(s string) *string { return &s }

