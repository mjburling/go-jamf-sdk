package jamf

import (
	"encoding/xml"
	"fmt"
)

// Script represents a jamf script object
type Script struct {
	XMLName               xml.Name             `xml:"script"`
	ID                    *int                 `xml:"id"`
	// TODO uniqueness constraint checking?
	Name                  *string              `xml:"name"`
	Category              *string              `xml:"category"`
	Filename              *string              `xml:"filename"`
	Info                  *string              `xml:"info"`
	Notes                 *string              `xml:"notes"`
	OSRequirements        *string              `xml:"os_requirements"`
	// TODO nested structure
	// Parameters            *XMLScriptParameters `xml:"parameters"`
	Priority              *string              `xml:"priority"`
	ScriptContents        *string              `xml:"script_contents"`
	// TODO requires constraints checking
	ScriptContentsEncoded *string              `xml:"script_contents_encoded"`
}

// TODO
// ScriptParameters represents the nested parameters object inside of a jamf script object
// type ScriptParameters struct {
// 	Parameter4  *string `xml:"parameter4"`
// 	Parameter5  *string `xml:"parameter5"`
// 	Parameter6  *string `xml:"parameter6"`
// 	Parameter7  *string `xml:"parameter7"`
// 	Parameter8  *string `xml:"parameter8"`
// 	Parameter9  *string `xml:"parameter9"`
// 	Parameter10 *string `xml:"parameter10"`
// 	Parameter11 *string `xml:"parameter11"`
// }

// GetScript returns a single script by given id
func (client *Client) GetScript(id int) (*Script, error) {
	var out Script
	uri := fmt.Sprintf("/scripts/id/%d", id)
	if err := client.doXMLRequest("GET", uri, nil, &out); err != nil {
		return nil, err
	}
	return &out, nil
}

// UpdateScript updates the remote script with given Script struct
func (client *Client) UpdateScript(script *Script) error {
	uri := fmt.Sprintf("/scripts/id/%d", *script.ID)
	return client.doXMLRequest("PUT", uri,
		script, nil)
}

// CreateScript creates a new script with given Script struct
func (client *Client) CreateScript(script *Script) (*Script, error) {
	var out Script
	if err := client.doXMLRequest("POST", "/scripts/id/0", script, &out); err != nil {
		return nil, err
	}
	return &out, nil
}

// DeleteScript deletes a script by a given id
func (client *Client) DeleteScript(id int) error {
	uri := fmt.Sprintf("/scripts/id/%d", id)
	return client.doXMLRequest("DELETE", uri, nil, nil)
}
