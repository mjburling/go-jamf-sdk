package jamf

import (
	"crypto/tls"
	"net/http"
	"os"
)

// Client is the object that handles talking to the Jamf API
type Client struct {
	username, password, baseURL string

	//The HTTP Client that is used to make requests
	HTTPClient *http.Client
}

// NewClient returns a new Client which can be used to access the API
func NewClient(username, password, baseURL string) *Client {

	_, insecure := os.LookupEnv("JAMF_INSECURE")

	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: insecure},
	}

	httpClient := &http.Client{Transport: tr}

	return &Client{
		username:   username,
		password:   password,
		baseURL:     baseURL,
		HTTPClient: httpClient,
	}
}

// SetCreds changes the value of user and password.
func (c *Client) SetCreds(username, password string) {
	c.username = username
	c.password = password
}

// SetBasURL changes the value of baseURL.
func (c *Client) SetBaseURL(baseURL string) {
	c.baseURL = baseURL
}

// GetBasURL returns the baseURL.
func (c *Client) GetBaseURL() string {
	return c.baseURL
}
