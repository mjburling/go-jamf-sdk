package jamf

import (
	"encoding/xml"
	"fmt"
)

// Category represents a jamf category object
type Category struct {
	XMLName  xml.Name `xml:"category"`
	ID       *int     `xml:"id"`
	Name     *string  `xml:"name"`
	Priority *string  `xml:"priority"`
}

// GetCategory returns a single category by given id
func (client *Client) GetCategory(id int) (*Category, error) {
	var out Category
	uri := fmt.Sprintf("/categories/id/%d", id)
	if err := client.doXMLRequest("GET", uri, nil, &out); err != nil {
		return nil, err
	}
	return &out, nil
}

// UpdateCategory updates the remote category with given Category struct
func (client *Client) UpdateCategory(category *Category) error {
	uri := fmt.Sprintf("/categories/id/%d", *category.ID)
	return client.doXMLRequest("PUT", uri,
		category, nil)
}

// CreateCategory creates a new category with given Category struct
func (client *Client) CreateCategory(category *Category) (*Category, error) {
	var out Category
	if err := client.doXMLRequest("POST", "/categories/id/0", category, &out); err != nil {
		return nil, err
	}
	return &out, nil
}

// DeleteCategory deletes a category by a given id
func (client *Client) DeleteCategory(id int) error {
	uri := fmt.Sprintf("/categories/id/%d", id)
	return client.doXMLRequest("DELETE", uri, nil, nil)
}
